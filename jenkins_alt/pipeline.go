package main

import (
	"fmt"
	//"bufio"
	//"bytes"
	//"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"io"
	//"io/ioutil"
	"log"
	"os"
	"time"
	//"errors"
	//"os/exec"
	/*#"path/filepath"
	#"strconv"
	#"strings"
	"time"

	#"github.com/prologic/bitcask"
	*/
)

func stopwatch(start time.Time, name string) {
    elapsed := time.Since(start)
    log.Printf("%s took %s", name, elapsed)
}


func CalculateHash(absoluteFilePath string) string {
    defer stopwatch(time.Now(), "benchmark")
    f, err := os.Open(absoluteFilePath)
    //HandleError(err)
    if err != nil {
        log.Fatal(err)
    }
    defer f.Close()
    h := sha256.New()
    if _, err := io.Copy(h, f); err != nil {
        log.Fatal(err)
    }
    return hex.EncodeToString(h.Sum(nil))
}

func main() {
	fmt.Println("Hello, world.")
	fmt.Println(CalculateHash("../../simple_CICD/deploy.yml"))
}
